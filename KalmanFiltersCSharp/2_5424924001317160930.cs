﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SKMNamespace
{
	static class UdpTest
	{
		static UdpClient _udpClient;
		private static int _lastPacketId;
		private static readonly object _lastPacketIdLocker;

		private const string UdpReceivrAddr = "127.0.0.1";
		private const int UdpReceivrPort = 1234;


		private static void CreateUdpServer()
		{
			_udpClient = new UdpClient();
		}
		private static void CloseSession()
		{
			try
			{
				_udpClient?.Close();
			}
			catch { }
		}



		private static int GetNextPacketId()
		{
			int packetId;
			lock (_lastPacketIdLocker)
			{
				packetId = ++_lastPacketId;
			}

			return packetId;
		}



		public static string CreatePacket()
		{
			int packetId = GetNextPacketId();
			var packet = $"{packetId};1;1;2;3;4;3";
			return packet;
		}

		public static void Test()
		{
			var packet = CreatePacket();
			Send(packet);
		}

		public static void Send(string packet)
		{
			Send(UdpReceivrAddr, UdpReceivrPort, packet);
		}

		private static void Send(string ip, int port, string packet)
		{
			Send(ip, port, Encoding.ASCII.GetBytes(packet + "\0"));
		}

		private static void Send(string ip, int port, byte[] bytes)
		{
			UdpClient udpClient = new UdpClient();
			udpClient.Send(bytes, bytes.Length, ip, port);
		}

	}
}
