﻿

namespace MatLabProject
{
    public struct ParsedLog
    {
        public string Id; 
        
        /// <summary>
        /// Time stamp
        /// </summary>
        public double Ts; 
        
        /// <summary>
        /// Message id
        /// </summary>
        public int Mid;
        
        /// <summary>
        /// Accelerometer
        /// </summary>
        public string A; 
        
        /// <summary>
        /// Gyro
        /// </summary>
        public string G; 
        
        /// <summary>
        /// Magnetometer
        /// </summary>
        public double[] M;
        
        /// <summary>
        /// Pressure
        /// </summary>
        public double P; 
        
        public struct DdLog
        {
            /// <summary>
            /// Names
            /// </summary>
            public string[] A; //names
            
            /// <summary>
            /// Delta distance
            /// </summary>
            public double[] Dd; //delta distance
        }

        public DdLog Meas;

    }
}