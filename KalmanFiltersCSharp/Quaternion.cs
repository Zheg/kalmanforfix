﻿﻿using System;
using System.Threading;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MatLabProject
{
    public static class Quaternion
    {
        /// <summary>
        /// Multiply two quternions
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Matrix<double> Multiply(Matrix<double> lhs, Matrix<double> rhs)
        {
            double[] temp = new double[4];
            temp[0] = lhs[0, 0] * rhs[0, 0] - lhs[1, 0] * rhs[1, 0] - lhs[2, 0] * rhs[2, 0] - lhs[3, 0] * rhs[3, 0];
            temp[1] = lhs[1, 0] * rhs[0, 0] + lhs[0, 0] * rhs[1, 0] - lhs[3, 0] * rhs[2, 0] + lhs[2, 0] * rhs[3, 0];
            temp[2] = lhs[2, 0] * rhs[0, 0] + lhs[3, 0] * rhs[1, 0] + lhs[0, 0] * rhs[2, 0] - lhs[1, 0] * rhs[3, 0];
            temp[3] = lhs[3, 0] * rhs[0, 0] - lhs[2, 0] * rhs[1, 0] + lhs[1, 0] * rhs[2, 0] + lhs[0, 0] * rhs[3, 0];
            return Matrix<double>.Build.DenseOfColumnArrays(temp);
        }

        /// <summary>
        /// Conjugate quaternion without editing input
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Matrix<double> Conjugate(Matrix<double> q)
        {
            var result = Matrix<double>.Build.DenseOfMatrix(q);
            result[1, 0] = -result[1, 0];
            result[2, 0] = -result[2, 0];
            result[3, 0] = -result[3, 0];
            return result;
        }

        /// <summary>
        /// Return rotation matrix of quaternion
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Matrix<double> ToRotationMatrix(Matrix<double> q)
        {
            Matrix<double> temp = Matrix<double>.Build.Dense(3, 3, .0);

            temp[0, 0] = (Math.Pow(q[0, 0], 2) + Math.Pow(q[1, 0], 2) - Math.Pow(q[2, 0], 2) - Math.Pow(q[3, 0], 2));
            temp[1, 1] =  (Math.Pow(q[0, 0], 2) - Math.Pow(q[1, 0], 2) + Math.Pow(q[2, 0], 2) - Math.Pow(q[3, 0], 2));
            temp[2, 2] =  (Math.Pow(q[0, 0], 2) - Math.Pow(q[1, 0], 2) - Math.Pow(q[2, 0], 2) + Math.Pow(q[3, 0], 2));

            temp[0, 1] = 2 * (q[1, 0] * q[2, 0] - q[0, 0] * q[3, 0]);
            temp[1, 0] = 2 * (q[1, 0] * q[2, 0] + q[0, 0] * q[3, 0]);

            temp[0, 2] = 2 * (q[1, 0] * q[3, 0] + q[0, 0] * q[2, 0]);
            temp[2, 0] = 2 * (q[1, 0] * q[3, 0] - q[0, 0] * q[2, 0]);

            temp[1, 2] = 2 * (q[2, 0] * q[3, 0] - q[0, 0] * q[1, 0]);
            temp[2, 1] = 2 * (q[2, 0] * q[3, 0] + q[0, 0] * q[1, 0]);

            return temp;
        }
    }
        
}