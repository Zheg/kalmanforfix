﻿﻿using System;
using MathNet.Numerics.Providers.LinearAlgebra;

namespace FilterQuatSkmRealTime
{
    public static class Procedures
    {
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        public static double Norm(double[] vector)
        {
            double sqrSum = 0;
            foreach (var val in vector)
            {
                sqrSum += val * val;
            }

            return Math.Sqrt(sqrSum);
        }
    }
}