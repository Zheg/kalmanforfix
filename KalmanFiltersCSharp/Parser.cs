﻿﻿using System;
using System.Globalization;
using Newtonsoft.Json;

namespace MatLabProject
{
    /// <summary>
    /// Parser for json format of logs
    /// </summary>
    public static class Parser
    {
        /// <summary>
        /// Correct sequence of names
        /// </summary>
        private static readonly string[] NAMES = {"4930", "499", "4ba4", "4824", "4b05", "4bab", "489d", "408"};
        
        /// <summary>
        /// Parse json string from file
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Log ParseLogString(string s)
        {
            var result = new Log();
            try
            {
                ParsedLog parsedLog = JsonConvert.DeserializeObject<ParsedLog>(s);
                result = RefactorLog(parsedLog);
            }
            catch (JsonException e)
            {
                result.Correct = false;
            }
            return result;
        }
        /// <summary>
        /// Convert Parsed log from file to needed log
        /// </summary>
        /// <param name="pLog"></param>
        /// <returns></returns>
        private static Log RefactorLog(ParsedLog pLog)
        {
            var result = new Log
            {
                Correct = false,
                DdInclude = false,
                MagInclude = false,
                AccInclude = false,
                GyroInclude = false,
                Id = pLog.Id,
                Ts = pLog.Ts,
                Mid = pLog.Mid
            };
            if (pLog.A != null)
            {
                try
                {
                    result.Acc = ToArrayDoubles(pLog.A);
                    result.AccInclude = true;
                    if (pLog.G != null)
                    {
                        result.Gyro = ToArrayDoubles(pLog.G);
                        for (int i = 0; i < result.Gyro.Length; ++i)
                        {
                            result.Gyro[i] *= (Math.PI / 180);
                        }
                        result.GyroInclude = true;
                    }

                }
                catch (Exception e)
                {
                    result.Correct = false;
                    return result;
                }
                if (pLog.M != null && pLog.M.Length == 3)
                {
                    result.MagInclude = true;
                    result.Mag = new double[3];
                    result.Mag[0] = pLog.M[2];
                    result.Mag[1] = pLog.M[0];
                    result.Mag[2] = -pLog.M[1];
                }
            } 
            else if (pLog.Meas.Dd != null)
            {
                result.DdInclude = true;
                result.Dd = new double[8];
                for (var i = 0; i < pLog.Meas.A.Length; ++i)
                {
                    var index = WhichOne(pLog.Meas.A[i]);
                    if (index == -1)
                    {
                        result.Correct = false;
                        return result;
                    }
                    else
                    {
                        result.Dd[index] = pLog.Meas.Dd[i];
                    }
                }
            }
            else
            {
                result.Correct = false;
                return result;
            }

            result.Correct = true;
            return result;
        }

        /// <summary>
        /// Convert string with [ ] brackets to array of doubles
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static double[] ToArrayDoubles(string s)
        {
            string[] temp = s.Substring(1, s.Length - 2).Split(',');
            double[] result = new double[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                result[i] = Double.Parse(temp[i], System.Globalization.CultureInfo.InvariantCulture);
            }
            
            return result;
            //return Array.ConvertAll(s.Substring(1, s.Length - 2).Split(','), double.Parse);
        }
        
        public static float[] ToArrayFloats(string s)
        {
            string[] temp = s.Substring(1, s.Length - 2).Split(',');
            float[] result = new float[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                result[i] = float.Parse(temp[i], System.Globalization.CultureInfo.InvariantCulture);
            }
            
            return result;
            //return Array.ConvertAll(s.Substring(1, s.Length - 2).Split(','), double.Parse);
        }

        /// <summary>
        /// Convert string with [ ] brackets to array of strings
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static string[] ToArrayStrings(string s)
        {
            return s.Substring(1, s.Length - 2).Split(',');
        }

        private static int WhichOne(string name)
        {
            int index = 0;
            while (index < NAMES.Length && name != NAMES[index])
            {
                ++index;
            }

            if (index == NAMES.Length)
            {
                return -1;
            }
            else
            {
                return index;
            }
        }
    }
}