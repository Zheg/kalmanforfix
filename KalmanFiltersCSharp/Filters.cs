﻿﻿using System;
 using System.Runtime.Serialization.Formatters;
 using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MatLabProject;

namespace FilterQuatSkmRealTime
{
    public static class Filters
    {
        public static MatrixBuilder<double> MB = Matrix<double>.Build;

        public static Tuple<Matrix<double>, Matrix<double>> KalmanDr(Matrix<double> r, Matrix<double> x_in,
            Matrix<double> Rn, Matrix<double> Q,
            Matrix<double> M, double T, bool raim_flag)
        {
            var F = Matrix<double>.Build.DenseIdentity(x_in.RowCount);
            var G = Matrix<double>.Build.DenseIdentity(x_in.RowCount) * T;
            var Dn = Matrix<double>.Build.Dense(Rn.RowCount, Rn.RowCount, 0.0);
            for (var i = 0; i < Rn.RowCount; ++i) Dn[i, i] = Rn[i, 0] * Rn[i, 0];

            var Dq = Matrix<double>.Build.Dense(x_in.RowCount, x_in.RowCount, 0.0);
            for (var i = 0; i < x_in.RowCount; ++i) Dq[i, i] = Q[i, 0];

            var x_ext = F * x_in;
            var M_ext = F * M * F.Transpose() + G * Dq * G.Transpose();
            var H = F;
            var r_ext = H * x_ext;
            var D = r - r_ext;
            if (raim_flag)
                for (var i = 0; i < D.RowCount; ++i)
                    if (Math.Abs(D[i, 0]) > 0.5)
                        D[i, 0] = 0;

            var S = H * M_ext * H.Transpose() + Dn;
            var K = M_ext * H.Transpose() * S.Conjugate();
            return new Tuple<Matrix<double>, Matrix<double>>(x_ext + K * D, M_ext - K * H * M_ext);
        }

        public static Tuple<Matrix<double>, Matrix<double>> kalman_quat5(Matrix<double> dr, Matrix<double> x_in,
            Matrix<double> Rn, Matrix<double> Q, Matrix<double> M, double T, double[] a, double[] w,
            double[] g, bool rad_flag, Matrix<double> SatPos, Matrix<double> q)
        {
            var F = MB.DenseIdentity(x_in.RowCount);
            var G = MB.Dense(x_in.RowCount, 7, 0.0);
            for (int i = 0; i < 7; i++)
            {
                G[i, i] = T;
            }

            var Dq = MB.Dense(7, 7, 0.0);
            for (int i = 0; i < 7; ++i)
            {
                Dq[i, i] = Q[i, 0];    
            }

            var x_ext = F * x_in;
            var M_ext=F*M*F.Transpose()+G*Dq*G.Transpose();


            if (!rad_flag)
            {
                var Dn = MB.Dense(4, 4, 0.0);
                for (int i = 0; i < 4; ++i)
                {
                    Dn[i, i] = Rn[i, 0] * Rn[i, 0];
                }

                var H = MB.Dense(4, 7, 0.0);
                for (int i = 0; i < 4; ++i)
                {
                    H[i, i + 3] = 1;
                }

                var r_ext_q = MB.Dense(4, 1);
                for (int i = 0; i < 4; i++)
                {
                    r_ext_q[i, 0] = x_ext[i + 3, 0];
                }

                var r = q;
                var D = r - r_ext_q;
                var S=H*M_ext*H.Transpose()+Dn;

                var K=M_ext*H.Transpose()  * S.Inverse();

                Console.WriteLine(K * D);
                M = M_ext - K * H * M_ext;
                var x_est = x_ext + K * D;
                return new Tuple<Matrix<double>, Matrix<double>>(x_est, M);
            }
            else
            {
                var Dn = MB.Dense(Rn.RowCount, Rn.RowCount, 0.0);
                for (int i = 0; i < Rn.RowCount; ++i)
                {
                    Dn[i, i] = Rn[i, 0] * Rn[i, 0];
                }
                var Hx = MB.Dense(dr.RowCount, 1);
                var Hy = MB.Dense(dr.RowCount, 1);
                var Hz = MB.Dense(dr.RowCount, 1);
                var x_ext_part_vec = Vector.Build.Dense(3);
                for (var i = 0; i < 3; i++) x_ext_part_vec[i] = x_ext[i, 0];
                for (int i = 0; i < dr.RowCount; ++i)
                {
                    
                    Hx[i, 0] = (x_ext[0, 0] - SatPos[0,i+1])/ (x_ext_part_vec - SatPos.Column(i + 1)).L2Norm() - (x_ext[0, 0] - SatPos[0, 0])/(x_ext_part_vec - SatPos.Column(i + 1)).L2Norm();
                    Hy[i, 0] = (x_ext[1, 0] - SatPos[1,i+1])/ (x_ext_part_vec - SatPos.Column(i + 1)).L2Norm() - (x_ext[1, 0] - SatPos[1, 0])/(x_ext_part_vec - SatPos.Column(i + 1)).L2Norm();
                    Hz[i, 0] = (x_ext[2, 0] - SatPos[2,i+1])/ (x_ext_part_vec - SatPos.Column(i + 1)).L2Norm() - (x_ext[2, 0] - SatPos[2, 0])/(x_ext_part_vec - SatPos.Column(i + 1)).L2Norm();
                }

                var H = MB.Dense(4 + dr.RowCount, 7, 0.0);
                for (int i = 0; i < 4; ++i)
                {
                    H[i, i + 3] = 1;
                }

                for (int i = 0; i < dr.RowCount; ++i)
                {
                    H[i + 4, 0] = Hx[i, 0];
                    H[i + 4, 1] = Hy[i, 0];
                    H[i + 4, 2] = Hz[i, 0];
                }

                var r_ext_dr = MB.Dense(dr.RowCount, 1, 0.0);
                for (int i = 0; i < dr.RowCount; i++)
                {
                    r_ext_dr[i, 0] = (x_ext_part_vec - SatPos.Column(i + 1)).L2Norm() -
                                     (x_ext_part_vec - SatPos.Column(0)).L2Norm();
                }
                var r_ext_q = MB.Dense(4, 1);
                for (int i = 0; i < 4; i++)
                {
                    r_ext_q[i, 0] = x_ext[i + 3, 0];
                }

                var r_ext = MB.Dense(r_ext_q.RowCount + r_ext_dr.RowCount, 1, 0.0);
                for (int i = 0; i < r_ext_q.RowCount; ++i)
                {
                    r_ext[i, 0] = r_ext_q[i, 0];
                }

                for (int i = 0; i < r_ext_dr.RowCount; ++i)
                {
                    r_ext[i + r_ext_q.RowCount, 0] = r_ext_dr[i, 0];
                }

                var r = MB.Dense(q.RowCount + dr.RowCount, 1);
                for (int i = 0; i < q.RowCount; ++i)
                {
                    r[i, 0] = q[i, 0];
                }

                for (int i = 0; i < dr.RowCount; ++i)
                {
                    r[i + q.RowCount, 0] = dr[i, 0];
                }

                var D = r - r_ext;
                var S=H*M_ext*H.Transpose()+Dn;
                var K=M_ext*H.Transpose()  * S.Inverse();
                return new Tuple<Matrix<double>, Matrix<double>>(x_ext + K*D, M_ext-K*H*M_ext);
            }
        }

        public static Tuple<Matrix<double>, Matrix<double>> kalman_acc_dus1(Matrix<double> dr, Matrix<double> x_in,
            Matrix<double> Rn, Matrix<double> Q, Matrix<double> M, double T, double[] a, double[] w,
            double[] g, bool rad_flag, Matrix<double> SatPos, Matrix<double> q)
        {
            var F = MB.DenseIdentity(x_in.RowCount);

            F[0, 3] = T;
            F[0, 19] = T * T /2;
            F[1, 4] = T;
            F[1, 20] = T* T/2;
            F[2, 5] = T;
            F[3, 21] = T* T/2;
            F[3, 19] = T;
            F[4, 20] = T;
            F[5, 21] = T;
            F[12 ,13] = T/2*(w[0] - 0*x_in[16, 0]);
            F[12 ,14] = T/2*(w[1] - 0*x_in[17, 0]);
            F[12 ,15] = T/2*(w[2] - 0*x_in[18, 0]);
            F[12 ,16] = -T/2*x_in[13, 0];
            F[12 ,17] = -T/2*x_in[14, 0];
            F[12 ,18] = -T/2*x_in[15, 0];
            F[13 ,12] =  T/2*(w[0] - 0*x_in[16, 0]);
            F[13 ,13] = -T/2*(w[1] - 0*x_in[17, 0]);
            F[13 ,14] =  T/2*(w[2] - 0*x_in[18, 0]);
            F[13 ,16] = -T/2*x_in[12, 0];
            F[13 ,17] = -T/2*x_in[15, 0];
            F[13 ,18] =  T/2*x_in[14, 0];
            F[14 ,12] =  T/2*(w[1] - 0*x_in[17, 0]);
            F[14 ,13] =  T/2*(w[2] - 0*x_in[18, 0]);
            F[14 ,14] = -T/2*(w[0] - 0*x_in[16, 0]);
            F[14 ,16] =  T/2*x_in[15, 0];
            F[14 ,17] = -T/2*x_in[12, 0];
            F[14 ,18] = -T/2*x_in[13, 0];
            F[15 ,12] =  T/2*(w[2] - 0*x_in[18, 0]);
            F[15 ,13] = -T/2*(w[1] - 0*x_in[17, 0]);
            F[15 ,14] =  T/2*(w[0] - 0*x_in[16, 0]);
            F[15 ,16] = -T/2*x_in[14, 0];
            F[15 ,17] =  T/2*x_in[13, 0];
            F[15 ,18] = -T/2*x_in[12, 0];

            var G= MB.Dense(x_in.RowCount, 12);
            G[9,0] = T;
            G[10,1] = T;
            G[11,2] = T;
            G[16,3] = T;
            G[17,4] = T;
            G[18,5] = T;
            G[19,6] = T;
            G[20,7] = T;
            G[22,8] = T;

            var Dq = MB.Dense(12, 12, 0.0);

            for (int i = 0; i < 12; ++i)
            {
                Dq[i, i] = Q[i, 0];
            }


            var x_ext=F*x_in;

            x_ext[12, 0] = x_in[12, 0] + T/2*((w[0])*x_in[13, 0] + (w[1])*x_in[14, 0] + (w[2])*x_in[15, 0]);
            x_ext[13, 0] = x_in[13, 0] + T/2*((w[0])*x_in[12, 0] - (w[2])*x_in[14, 0] + (w[1])*x_in[15, 0]);
            x_ext[14, 0] = x_in[14, 0] + T/2*((w[1])*x_in[12, 0] + (w[2])*x_in[13, 0] - (w[0])*x_in[15, 0]);
            x_ext[15, 0] = x_in[15, 0] + T/2*((w[2])*x_in[12, 0] - (w[1])*x_in[13, 0] + (w[0])*x_in[14, 0]);
            
            var M_ext=F*M*F.Transpose()+G*Dq*G.Transpose();
            if (rad_flag)
            {
                var Dn = MB.Dense(dr.RowCount + 11, dr.RowCount + 11);
                for (int i = 0; i < Dn.RowCount; ++i)
                {
                    Dn[i, i] = Rn[i, 0] * Rn[i, 0];
                }

                var H = MB.Dense(dr.RowCount + 7, x_in.RowCount);
                var Hx = MB.Dense(dr.RowCount, 1);
                var Hy = MB.Dense(dr.RowCount, 1);
                var Hz = MB.Dense(dr.RowCount, 1);
                var x_ext_part_vec = Vector.Build.Dense(3);
                for (var i = 0; i < 3; i++) x_ext_part_vec[i] = x_ext[i, 0];
                for (int i = 0; i < dr.RowCount; ++i)
                {
                    
                    Hx[i, 0] = (x_ext[1, 0] - SatPos[1,i+1])/ (x_ext_part_vec - SatPos.Column(i + 1)).L2Norm() - (x_ext[1, 0] - SatPos[1, 0])/(x_ext_part_vec - SatPos.Column(i + 1)).L2Norm();
                    Hy[i, 0] = (x_ext[2, 0] - SatPos[2,i+1])/ (x_ext_part_vec - SatPos.Column(i + 1)).L2Norm() - (x_ext[2, 0] - SatPos[2, 0])/(x_ext_part_vec - SatPos.Column(i + 1)).L2Norm();
                    Hz[i, 0] = (x_ext[3, 0] - SatPos[3,i+1])/ (x_ext_part_vec - SatPos.Column(i + 1)).L2Norm() - (x_ext[3, 0] - SatPos[3, 0])/(x_ext_part_vec - SatPos.Column(i + 1)).L2Norm();
                }

                for (int i = 0; i < dr.RowCount; ++i)
                {
                    H[i, 0] = Hx[i, 0];
                    H[i, 1] = Hy[i, 0];
                    H[i, 2] = Hz[i, 0];
                }
                
                H[dr.RowCount+0,19] = x_ext[12, 0] * x_ext[12, 0] + x_ext[13, 0]* x_ext[13, 0]- x_ext[14, 0]*x_ext[14, 0] - x_ext[15, 0]*x_ext[15, 0];
                H[dr.RowCount+0,20] = 2*(x_ext[13, 0]*x_ext[14, 0] - x_ext[12, 0]*x_ext[15, 0]);
                H[dr.RowCount+0,21] = 2*(x_ext[13, 0]*x_ext[15, 0] + x_ext[12, 0]*x_ext[14, 0]);
                H[dr.RowCount+0,9] =  H[dr.RowCount+0,19];
                H[dr.RowCount+0,10] = H[dr.RowCount+0,20];
                H[dr.RowCount+0,11] = H[dr.RowCount+0,21];
                H[dr.RowCount+0,6] =  H[dr.RowCount+0,19];
                H[dr.RowCount+0,7] =  H[dr.RowCount+0,20];
                H[dr.RowCount+0,8] =  H[dr.RowCount+0,21];
                H[dr.RowCount+0,12] =  2*x_ext[12, 0]*(x_ext[19, 0] + x_ext[6, 0] + x_ext[9, 0]) - 2*x_ext[15, 0]*(x_ext[20, 0] + x_ext[7, 0] + x_ext[10, 0]) + 2*x_ext[14, 0]*(x_ext[21, 0] + x_ext[8, 0] + x_ext[11, 0]);
                H[dr.RowCount+0,13] =  2*x_ext[13, 0]*(x_ext[19, 0] + x_ext[6, 0] + x_ext[9, 0]) + 2*x_ext[14, 0]*(x_ext[20, 0] + x_ext[7, 0] + x_ext[10, 0]) + 2*x_ext[15, 0]*(x_ext[21, 0] + x_ext[8, 0] + x_ext[11, 0]);
                H[dr.RowCount+0,14] = -2*x_ext[14, 0]*(x_ext[19, 0] + x_ext[6, 0] + x_ext[9, 0]) + 2*x_ext[13, 0]*(x_ext[20, 0] + x_ext[7, 0] + x_ext[10, 0]) + 2*x_ext[12, 0]*(x_ext[21, 0] + x_ext[8, 0] + x_ext[11, 0]);
                H[dr.RowCount+0,15] = -2*x_ext[15, 0]*(x_ext[19, 0] + x_ext[6, 0] + x_ext[9, 0]) - 2*x_ext[12, 0]*(x_ext[20, 0] + x_ext[7, 0] + x_ext[10, 0]) + 2*x_ext[13, 0]*(x_ext[21, 0] + x_ext[8, 0] + x_ext[11, 0]);
                // H[dr.RowCount+1,19] =  2*(x_ext(14)*x_ext(15) + x_ext(13)*x_ext(16));
                // H[dr.RowCount+1,20] = x_ext[12, 0] * x_ext[12, 0] + x_ext[13, 0]* x_ext[13, 0]- x_ext[14, 0]*x_ext[14, 0] - x_ext[15, 0]*x_ext[15, 0];
                // H[dr.RowCount+1,21] =  2*(x_ext(15)*x_ext(16) - x_ext(13)*x_ext(14));
                // H[dr.RowCount+1,9] =  H[dr.RowCount+1,19];
                // H[dr.RowCount+1,10] = H[dr.RowCount+1,20];
                // H[dr.RowCount+1,11] = H[dr.RowCount+1,21];
                // H[dr.RowCount+1,6] = H[dr.RowCount+1,19];
                // H[dr.RowCount+1,7] = H[dr.RowCount+1,20];
                // H[dr.RowCount+1,8] = H[dr.RowCount+1,21];
                // H[dr.RowCount+1,12] = 2*x_ext(16)*(x_ext(20) + x_ext(7) + x_ext(10)) + 2*x_ext(13)*(x_ext(21) + x_ext(8) + x_ext(11)) - 2*x_ext(14)*(x_ext(22) + x_ext(9) + x_ext(12));
                // H[dr.RowCount+1,13] = 2*x_ext(15)*(x_ext(20) + x_ext(7) + x_ext(10)) - 2*x_ext(14)*(x_ext(21) + x_ext(8) + x_ext(11)) - 2*x_ext(13)*(x_ext(22) + x_ext(9) + x_ext(12));
                // H[dr.RowCount+1,14] = 2*x_ext(14)*(x_ext(20) + x_ext(7) + x_ext(10)) + 2*x_ext(15)*(x_ext(21) + x_ext(8) + x_ext(11)) + 2*x_ext(16)*(x_ext(22) + x_ext(9) + x_ext(12));
                // H[dr.RowCount+1,15] = 2*x_ext(13)*(x_ext(20) + x_ext(7) + x_ext(10)) - 2*x_ext(16)*(x_ext(21) + x_ext(8) + x_ext(11)) + 2*x_ext(15)*(x_ext(22) + x_ext(9) + x_ext(12));
                // H[dr.RowCount+2,19] = 2*(x_ext(14)*x_ext(16) - x_ext(13)*x_ext(15));
                // H[dr.RowCount+2,20] = 2*(x_ext(15)*x_ext(16) + x_ext(13)*x_ext(14));
                // H[dr.RowCount+2,21] = x_ext(13)^2 - x_ext(14)^2 - x_ext(15)^2 + x_ext(16)^2;
                // H[dr.RowCount+2,9] =  H[dr.RowCount+2,19];
                // H[dr.RowCount+2,10] = H[dr.RowCount+2,20];
                // H[dr.RowCount+2,11] = H[dr.RowCount+2,21];
                // H[dr.RowCount+2,6] =  H[dr.RowCount+2,19];
                // H[dr.RowCount+2,7] =  H[dr.RowCount+2,20];
                // H[dr.RowCount+2,8] =  H[dr.RowCount+2,21];
                // H[dr.RowCount+2,12] = -2*x_ext(15)*(x_ext(20) + x_ext(7) + x_ext(10)) + 2*x_ext(14)*(x_ext(21) + x_ext(8) + x_ext(11)) + 2*x_ext(13)*(x_ext(22) + x_ext(9) + x_ext(12));
                // H[dr.RowCount+2,13] =  2*x_ext(16)*(x_ext(20) + x_ext(7) + x_ext(10)) + 2*x_ext(13)*(x_ext(21) + x_ext(8) + x_ext(11)) - 2*x_ext(14)*(x_ext(22) + x_ext(9) + x_ext(12));
                // H[dr.RowCount+2,14] = -2*x_ext(13)*(x_ext(20) + x_ext(7) + x_ext(10)) + 2*x_ext(16)*(x_ext(21) + x_ext(8) + x_ext(11)) - 2*x_ext(15)*(x_ext(22) + x_ext(9) + x_ext(12));
                // H[dr.RowCount+2,15] =  2*x_ext(14)*(x_ext(20) + x_ext(7) + x_ext(10)) + 2*x_ext(15)*(x_ext(21) + x_ext(8) + x_ext(11)) + 2*x_ext(16)*(x_ext(22) + x_ext(9) + x_ext(12));
                // H[dr.RowCount+3,6] = 1;
                // H[dr.RowCount+4,7] = 1;
                // H[dr.RowCount+5,8] = 1;
            }
            else
            {
                
            }

            throw new NotImplementedException();
            return null;
        }

        public static Tuple<Matrix<double>, Matrix<double>> kalman_dr(Matrix<double> r, Matrix<double> x_in,
            Matrix<double> Rn, Matrix<double> Q,
            Matrix<double> M, double T, bool raim_flag)
        {
            var F = Matrix<double>.Build.DenseIdentity(x_in.RowCount);
            var G = Matrix<double>.Build.DenseIdentity(x_in.RowCount) * T;
            var Dn = Matrix<double>.Build.Dense(r.RowCount, r.RowCount, 0.0);
            for (var i = 0; i < r.RowCount; ++i)
            {
                for (int j = 0; j < r.RowCount; ++j)
                    if (i == j)
                    {
                        Dn[i, i] = Rn[i, 0] * Rn[i, 0];
                    }
                    else
                    {
                        Dn[i, j] = Rn[i, 0] * Rn[j, 0] / 1e1;
                    }
            }

            var Dq = Matrix<double>.Build.Dense(x_in.RowCount, x_in.RowCount, 0.0);
            for (var i = 0; i < x_in.RowCount; ++i)
            {
                for (int j = 0; j < x_in.RowCount; ++j)
                {
                    if (i == j)
                        Dq[i, i] = Q[i, 0];
                    else
                        Dq[i, j] = Q[i, 0] * Q[j, 0];
                }
            }

            var x_ext = F * x_in;
            var M_ext = F * M * F.Transpose() + G * Dq * G.Transpose();
            var H = F;
            var r_ext = H * x_ext;
            var D = r - r_ext;
            // if (raim_flag)
            //     for (var i = 0; i < D.RowCount; ++i)
            //         if (Math.Abs(D[i, 0]) > 0.5)
            //             D[i, 0] = 0;

            var S = H * M_ext * H.Transpose() + Dn;
            var K = M_ext * H.Transpose() * S.Conjugate();
            return new Tuple<Matrix<double>, Matrix<double>>(x_ext + K * D, M_ext - K * H * M_ext);
        }

        public static Tuple<Matrix<double>, Matrix<double>> kalman_quat3(Matrix<double> dr, Matrix<double> x_in,
            Matrix<double> Rn, Matrix<double> Q, Matrix<double> M, double T, double[] a, double[] w,
            double[] g, bool rad_flag, Matrix<double> SatPos)
        {
            var F = MB.DenseIdentity(x_in.RowCount);
            F[0, 1] = -T / 2 * (x_in[4, 0] - x_in[7, 0]);
            F[0, 2] = -T / 2 * (x_in[5, 0] - x_in[8, 0]);
            F[0, 3] = -T / 2 * (x_in[6, 0] - x_in[9, 0]);

            F[0, 4] = -T / 2 * x_in[1, 0];
            F[0, 5] = -T / 2 * x_in[2, 0];
            F[0, 6] = -T / 2 * x_in[3, 0];
            F[0, 7] = F[0, 4];
            F[0, 8] = F[0, 5];
            F[0, 9] = F[0, 6];

            F[1, 0] = T / 2 * (x_in[4, 0] - x_in[7, 0]);
            F[1, 2] = T / 2 * (x_in[6, 0] - x_in[9, 0]);
            F[1, 3] = -T / 2 * (x_in[5, 0] - x_in[8, 0]);

            F[1, 4] = T / 2 * x_in[0, 0];
            F[1, 5] = -T / 2 * x_in[3, 0];
            F[1, 6] = T / 2 * x_in[2, 0];
            F[1, 7] = F[1, 4];
            F[1, 8] = F[1, 5];
            F[1, 9] = F[1, 6];

            F[2, 0] = T / 2 * (x_in[5, 0] - x_in[8, 0]);
            F[2, 1] = -T / 2 * (x_in[6, 0] - x_in[9, 0]);
            F[2, 3] = T / 2 * (x_in[4, 0] - x_in[7, 0]);

            F[2, 4] = T / 2 * x_in[3, 0];
            F[2, 5] = T / 2 * x_in[0, 0];
            F[2, 6] = -T / 2 * x_in[1, 0];
            F[2, 7] = F[2, 4];
            F[2, 8] = F[2, 5];
            F[2, 9] = F[2, 6];

            F[3, 0] = T / 2 * (x_in[6, 0] - x_in[9, 0]);
            F[3, 1] = T / 2 * (x_in[5, 0] - x_in[8, 0]);
            F[3, 2] = -T / 2 * (x_in[4, 0] - x_in[7, 0]);

            F[3, 4] = -T / 2 * x_in[2, 0];
            F[3, 5] = T / 2 * x_in[1, 0];
            F[3, 6] = T / 2 * x_in[0, 0];
            F[3, 7] = F[3, 4];
            F[3, 8] = F[3, 5];
            F[3, 9] = F[3, 6];

            for (var i = 13; i < 16; ++i)
            {
                F[i, i + 3] = T;
                F[i, i + 6] = T * T / 2;
                F[i + 3, i + 6] = T;
            }

            var G = MB.Dense(x_in.RowCount, 12, 0.0);
            for (var i = 0; i < 6; ++i) G[i + 4, i] = T;

            for (var i = 0; i < 3; ++i)
            {
                G[i + 19, i + 6] = T;
                G[i + 22, i + 9] = 1;
            }

            var Dq = MB.Dense(12, 12, 0.0);
            for (var i = 0; i < 12; i++) Dq[i, i] = Q[i, 0];

            var x_ext = F * x_in;
            x_ext[0, 0] = x_in[0, 0] + T / 2 * (-(w[0] + 0 * x_in[7, 0]) * x_in[1, 0] -
                                                (w[1] + 0 * x_in[8, 0]) * x_in[2, 0] -
                                                (w[2] + 0 * x_in[9, 0]) * x_in[3, 0]);
            x_ext[1, 0] = x_in[1, 0] + T / 2 * ((w[0] + 0 * x_in[7, 0]) * x_in[0, 0] +
                                                (w[2] + 0 * x_in[9, 0]) * x_in[2, 0] -
                                                (w[1] + 0 * x_in[8, 0]) * x_in[3, 0]);
            x_ext[2, 0] = x_in[2, 0] + T / 2 * ((w[1] + 0 * x_in[8, 0]) * x_in[0, 0] -
                                                (w[2] + 0 * x_in[9, 0]) * x_in[1, 0] +
                                                (w[0] + 0 * x_in[7, 0]) * x_in[3, 0]);
            x_ext[3, 0] = x_in[3, 0] + T / 2 * ((w[2] + 0 * x_in[9, 0]) * x_in[0, 0] +
                                                (w[1] + 0 * x_in[8, 0]) * x_in[1, 0] -
                                                (w[0] + 0 * x_in[7, 0]) * x_in[2, 0]);

            var M_ext = F * M * F.Transpose() + G * Dq * G.Transpose();

            Matrix<double> x_est;
            if (!rad_flag)
            {
                var Dn = MB.Dense(10, 10, 0.0);
                for (var i = 0; i < 10; ++i) Dn[i, i] = Rn[i, 0] * Rn[i, 0];

                var H = MB.Dense(10, 25, 0.0);
                var r_ext = MB.Dense(3 + 1 + 3 + 3, 1, 0.0);
                var r = MB.Dense(3 + 1 + 3 + 3, 1, 0.0);
                
                InitializeHandRExtAndR(ref H, ref r_ext, ref r, ref x_ext, ref w, ref a, ref g);

                var D = r - r_ext;
                var S = H * M_ext * H.Transpose() + Dn;
                var K = M_ext * H.Transpose() * S.Inverse();
                // Console.WriteLine(M_ext.Transpose().ToString(25, 25));
                // Console.WriteLine(H.Transpose().ToString(25, 25));
                // Console.WriteLine(S.ToString(25, 25));
                //
                M = M_ext - K * H * M_ext;
                x_est = x_ext + K * D;
            }
            else
            {
                var Dn = MB.Dense(Rn.RowCount, Rn.RowCount, 0.0);
                for (var i = 0; i < Rn.RowCount; ++i) Dn[i, i] = Rn[i, 0] * Rn[i, 0];


                var H = MB.Dense(10 + dr.RowCount, 25, 0.0);
                var r_ext = MB.Dense(3 + 1 + 3 + 3 + dr.RowCount, 1, 0.0);
                var r = MB.Dense(3 + 1 + 3 + 3 + +dr.RowCount, 1, 0.0);
                
                InitializeHandRExtAndR(ref H, ref r_ext, ref r_ext, ref x_ext, ref w, ref a, ref g);
                
                
                var Hx = new double[dr.RowCount];
                var Hy = new double[dr.RowCount];
                var Hz = new double[dr.RowCount];
                var x_ext_part = new double[3];
                for (var i = 0; i < 3; i++) x_ext_part[i] = x_ext[i, 0];
                var x_ext_part_mat = Vector<double>.Build.Dense(x_ext_part);
                for (var i = 0; i < dr.RowCount; i++)
                {
                    Hx[i] = (x_ext[0, 0] - SatPos[0, i + 1]) / (x_ext_part_mat - SatPos.Column(i + 1)).L2Norm() -
                            (x_ext[0, 0] - SatPos[0, 0]) / (x_ext_part_mat - SatPos.Column(0)).L2Norm();
                    Hy[i] = (x_ext[1, 0] - SatPos[1, i + 1]) / (x_ext_part_mat - SatPos.Column(i + 1)).L2Norm() -
                            (x_ext[1, 0] - SatPos[1, 0]) / (x_ext_part_mat - SatPos.Column(0)).L2Norm();
                    Hz[i] = (x_ext[2, 0] - SatPos[2, i + 1]) / (x_ext_part_mat - SatPos.Column(i + 1)).L2Norm() -
                            (x_ext[2, 0] - SatPos[2, 0]) / (x_ext_part_mat - SatPos.Column(0)).L2Norm();
                }

                for (var i = 0; i < dr.RowCount; i++)
                {
                    H[i + 10, 13] = Hx[i];
                    H[i + 10, 14] = Hy[i];
                    H[i + 10, 15] = Hz[i];
                }
                
                var x_ext_part_2 = new double[3];
                for (var i = 0; i < 3; ++i) x_ext_part_2[i] = x_ext[i + 13, 0];

                var x_ext_part2_vec = Vector.Build.Dense(x_ext_part_2);
                var r_ext_dr = new double[dr.RowCount];
                for (var i = 0; i < dr.RowCount; ++i)
                    r_ext_dr[i] = (x_ext_part2_vec - SatPos.Column(i + 1)).L2Norm() -
                                  (x_ext_part2_vec - SatPos.Column(0)).L2Norm();

                for (var i = 0; i < dr.RowCount; ++i)
                {
                    r_ext[i + 3 + 1 + 3 + 3, 0] = r_ext_dr[i];
                    r[i + 3 + 1 + 3 + 3, 0] = dr[i, 0];
                }

                var D = r - r_ext;

                var S = H * M_ext * H.Transpose() + Dn;

                var K = M_ext * H.Transpose() * S.Inverse();

                M = M_ext - K * H * M_ext;
                x_est = x_ext + K * D;
            }

            return new Tuple<Matrix<double>, Matrix<double>>(x_est, M);
        }

        private static void InitializeHandRExtAndR(ref Matrix<double> H, ref Matrix<double> r_ext, ref Matrix<double> r,
            ref Matrix<double> x_ext, ref double[] w, ref double[] a, ref double[] g)
        {
             for (var i = 0; i < 3; ++i)
             {
                 H[i, i + 4] = 1;
                 H[i, i + 7] = 1;
             }

             var x_ext_temp_part = new double[4];
             for (var i = 0; i < 4; ++i) x_ext_temp_part[i] = x_ext[i, 0];

             var r_ext_q = MB.DenseOfColumnArrays(x_ext_temp_part);
             var r_ext_n_q = r_ext_q.L2Norm();
             for (var i = 0; i < 4; ++i) H[3, i] = x_ext[i, 0] / r_ext_n_q;

             H[4, 0] = 2 * x_ext[0, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) -
                       2 * x_ext[3, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) +
                       2 * x_ext[2, 0] * (x_ext[12, 0] + x_ext[21, 0] + x_ext[24, 0]);
             H[4, 1] = 2 * x_ext[1, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) +
                       2 * x_ext[2, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) +
                       2 * x_ext[3, 0] * (x_ext[12, 0] + x_ext[21, 0] + x_ext[24, 0]);
             H[4, 2] = -2 * x_ext[2, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) +
                       2 * x_ext[1, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) +
                       2 * x_ext[0, 0] * (x_ext[12, 0] + x_ext[21, 0] + x_ext[24, 0]);
             H[4, 3] = -2 * x_ext[3, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) -
                       2 * x_ext[0, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) +
                       2 * x_ext[1, 0] * (x_ext[12, 0] + x_ext[21, 0] + x_ext[24, 0]);

             H[5, 0] = 2 * x_ext[3, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) +
                       2 * x_ext[0, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) -
                       2 * x_ext[1, 0] * (x_ext[12, 0] + x_ext[21, 0] + x_ext[24, 0]);
             H[5, 1] = 2 * x_ext[2, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) -
                       2 * x_ext[1, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) -
                       2 * x_ext[0, 0] * (x_ext[12, 0] + x_ext[21, 0] + x_ext[24, 0]);
             H[5, 2] = 2 * x_ext[1, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) +
                       2 * x_ext[2, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) +
                       2 * x_ext[3, 0] * (x_ext[12, 0] + x_ext[21, 0] + x_ext[24, 0]);
             H[5, 3] = 2 * x_ext[0, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) -
                       2 * x_ext[3, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) +
                       2 * x_ext[2, 0] * (x_ext[12, 0] + x_ext[21, 0] + x_ext[24, 0]);

             H[6, 0] = -2 * x_ext[2, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) +
                       2 * x_ext[1, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) +
                       2 * x_ext[0, 0] * (x_ext[12, 0] + x_ext[22, 0] + x_ext[24, 0]);
             H[6, 1] = 2 * x_ext[3, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) +
                       2 * x_ext[0, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) -
                       2 * x_ext[1, 0] * (x_ext[12, 0] + x_ext[22, 0] + x_ext[24, 0]);
             H[6, 2] = -2 * x_ext[0, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) +
                       2 * x_ext[3, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) -
                       2 * x_ext[2, 0] * (x_ext[12, 0] + x_ext[22, 0] + x_ext[24, 0]);
             H[6, 3] = 2 * x_ext[1, 0] * (x_ext[10, 0] + x_ext[19, 0] + x_ext[22, 0]) +
                       2 * x_ext[2, 0] * (x_ext[11, 0] + x_ext[20, 0] + x_ext[23, 0]) +
                       2 * x_ext[3, 0] * (x_ext[12, 0] + x_ext[22, 0] + x_ext[24, 0]);

             H[4, 10] = x_ext[0, 0] * x_ext[0, 0] + x_ext[1, 0] * x_ext[1, 0] - x_ext[2, 0] * x_ext[2, 0] -
                        x_ext[3, 0] * x_ext[3, 0];
             H[4, 11] = 2 * (x_ext[1, 0] * x_ext[2, 0] - x_ext[0, 0] * x_ext[3, 0]);
             H[4, 12] = 2 * (x_ext[1, 0] * x_ext[3, 0] + x_ext[0, 0] * x_ext[2, 0]);

             H[5, 10] = 2 * (x_ext[1, 0] * x_ext[2, 0] + x_ext[0, 0] * x_ext[3, 0]);
             H[5, 11] = x_ext[0, 0] * x_ext[0, 0] - x_ext[1, 0] * x_ext[1, 0] + x_ext[2, 0] * x_ext[2, 0] -
                        x_ext[3, 0] * x_ext[3, 0];
             H[5, 12] = 2 * (x_ext[2, 0] * x_ext[3, 0] - x_ext[0, 0] * x_ext[1, 0]);

             H[6, 10] = 2 * (x_ext[1, 0] * x_ext[3, 0] - x_ext[0, 0] * x_ext[2, 0]);
             H[6, 11] = 2 * (x_ext[2, 0] * x_ext[3, 0] + x_ext[0, 0] * x_ext[1, 0]);
             H[6, 12] = x_ext[0, 0] * x_ext[0, 0] - x_ext[1, 0] * x_ext[1, 0] - x_ext[2, 0] * x_ext[2, 0] +
                        x_ext[3, 0] * x_ext[3, 0];

             H[7, 10] = 1;
             H[8, 11] = 1;
             H[9, 12] = 1;

             H[4, 19] = x_ext[0, 0] * x_ext[0, 0] + x_ext[1, 0] * x_ext[1, 0] - x_ext[2, 0] * x_ext[2, 0] -
                        x_ext[3, 0] * x_ext[3, 0];
             H[4, 20] = 2 * (x_ext[1, 0] * x_ext[2, 0] - x_ext[0, 0] * x_ext[3, 0]);
             H[4, 21] = 2 * (x_ext[1, 0] * x_ext[3, 0] + x_ext[0, 0] * x_ext[2, 0]);

             H[5, 19] = 2 * (x_ext[1, 0] * x_ext[2, 0] + x_ext[0, 0] * x_ext[3, 0]);
             H[5, 20] = x_ext[0, 0] * x_ext[0, 0] - x_ext[1, 0] * x_ext[1, 0] + x_ext[2, 0] * x_ext[2, 0] -
                        x_ext[3, 0] * x_ext[3, 0];
             H[5, 21] = 2 * (x_ext[2, 0] * x_ext[3, 0] - x_ext[0, 0] * x_ext[1, 0]);

             H[6, 19] = 2 * (x_ext[1, 0] * x_ext[3, 0] - x_ext[0, 0] * x_ext[2, 0]);
             H[6, 20] = 2 * (x_ext[2, 0] * x_ext[3, 0] + x_ext[0, 0] * x_ext[1, 0]);
             H[6, 21] = x_ext[0, 0] * x_ext[0, 0] - x_ext[1, 0] * x_ext[1, 0] - x_ext[2, 0] * x_ext[2, 0] +
                        x_ext[3, 0] * x_ext[3, 0];

             H[4, 22] = x_ext[0, 0] * x_ext[0, 0] + x_ext[1, 0] * x_ext[1, 0] - x_ext[2, 0] * x_ext[2, 0] -
                        x_ext[3, 0] * x_ext[3, 0];
             H[4, 23] = 2 * (x_ext[1, 0] * x_ext[2, 0] - x_ext[0, 0] * x_ext[3, 0]);
             H[4, 24] = 2 * (x_ext[1, 0] * x_ext[3, 0] + x_ext[0, 0] * x_ext[2, 0]);

             H[5, 22] = 2 * (x_ext[1, 0] * x_ext[2, 0] + x_ext[0, 0] * x_ext[3, 0]);
             H[5, 23] = x_ext[0, 0] * x_ext[0, 0] - x_ext[1, 0] * x_ext[1, 0] + x_ext[2, 0] * x_ext[2, 0] -
                        x_ext[3, 0] * x_ext[3, 0];
             H[5, 24] = 2 * (x_ext[2, 0] * x_ext[3, 0] - x_ext[0, 0] * x_ext[1, 0]);

             H[6, 22] = 2 * (x_ext[1, 0] * x_ext[3, 0] - x_ext[0, 0] * x_ext[2, 0]);
             H[6, 23] = 2 * (x_ext[2, 0] * x_ext[3, 0] + x_ext[0, 0] * x_ext[1, 0]);
             H[6, 24] = x_ext[0, 0] * x_ext[0, 0] - x_ext[1, 0] * x_ext[1, 0] - x_ext[2, 0] * x_ext[2, 0] +
                        x_ext[3, 0] * x_ext[3, 0];
            
             var r_ext_g_h = new double[3];
             var r_ext_a1_h = new double[3];
             var r_ext_a2_h = new double[3];
             for (var i = 0; i < 3; i++)
             {
                 r_ext_g_h[i] = x_ext[i + 10, 0];
                 r_ext_a1_h[i] = x_ext[i + 19, 0];
                 r_ext_a2_h[i] = x_ext[i + 22, 0];
             }
            
             var r_ext_g = MB.DenseOfColumnArrays(r_ext_g_h);
             var r_ext_a1 = MB.DenseOfColumnArrays(r_ext_a1_h);
             var r_ext_a2 = MB.DenseOfColumnArrays(r_ext_a2_h);
             var r_ext_q_rot_mat = Quaternion.ToRotationMatrix(r_ext_q);
             var r_ext_a = r_ext_q_rot_mat * r_ext_g + r_ext_q_rot_mat * r_ext_a1 + r_ext_q_rot_mat * r_ext_a2;
             for (var i = 0; i < 3; ++i)
             {
                 r_ext[i, 0] = x_ext[i + 4, 0];
                 r_ext[i + 4, 0] = r_ext_a[i, 0];
                 r_ext[i + 7, 0] = r_ext_g[i, 0];

                 r[i, 0] = w[i];
                 r[i + 4, 0] = a[i];
                 r[i + 7, 0] = g[i];
             }

             r_ext[3, 0] = r_ext_n_q;
             r[3, 0] = 1;
        }
    }
}