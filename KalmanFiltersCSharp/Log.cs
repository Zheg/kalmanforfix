﻿﻿namespace MatLabProject
{
    public struct Log
    {
        public string Id;
        
        /// <summary>
        /// Time stamp
        /// </summary>
        public double Ts;
        
        /// <summary>
        /// Message id
        /// </summary>
        public int Mid;
        
        /// <summary>
        /// Accelerometer
        /// </summary>
        public double[] Acc; 
        
        /// <summary>
        /// Gyro: measure * pi / 180
        /// </summary>
        public double[] Gyro; // measure * pi / 180
        
        /// <summary>
        /// Magnetometer changed to right-haded vectors
        /// </summary>
        public double[] Mag; //reversed from LOG-file
        
        /// <summary>
        /// Delta distances sorted in concert with const NAMES
        /// </summary>
        public double[] Dd; //Sorted in concert with const NAMES
        
        /// <summary>
        /// If log is correct
        /// </summary>
        public bool Correct;
        
        /// <summary>
        /// Delta distances measures include
        /// </summary>
        public bool DdInclude;
        
        /// <summary>
        /// Magnetometer measures include
        /// </summary>
        public bool MagInclude;

        /// <summary>
        /// Gyro measures include
        /// </summary>
        public bool GyroInclude;
        
        /// <summary>
        /// Accelerometer measures include
        /// </summary>
        public bool AccInclude;
    }
}