﻿using System;
using System.Collections.Specialized;
using System.IO;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MatLabProject;
using SKMNamespace;

namespace FilterQuatSkmRealTime
{
    internal class Program
    {
        private static Matrix<double> DRn = 
            Vector.Build.Dense(new[] {0.0883, 0.0515, 0.0847, 0.1672, 0.1348, 0.5278 / 5, 0.0995}).ToColumnMatrix();
        private static Matrix<double> DQ = Vector.Build.Dense(7, 1.0 / 5).ToColumnMatrix();
        private static VectorBuilder<double> VB = Vector<double>.Build;
        private static MatrixBuilder<double> MB = Matrix<double>.Build;
        private static double TOLERANCE = 1e-16;

        public static void Main(string[] args)
        {
            DRn /= 2;
            var DM = Matrix.Build.DenseIdentity(DRn.RowCount);
            for (int i = 0; i < DRn.RowCount; ++i)
            {
                for (int j = 0; j < DRn.RowCount; ++j)
                {
                    if (i == j)
                        DM[i, i] = DRn[i, 0] / 1e2;
                    else
                        DM[i, j] = DRn[i, 0] * DRn[j, 0] / 1e2;
                }
            }
            var raimFlag = false;
            var x1 = Vector.Build.Dense(new[] {0, 5.154, 2.154});
            var x2 = Vector.Build.Dense(new[] {0, 5.154, 0.04});
            var x3 = Vector.Build.Dense(new[] {2.702, 5.146, 2.113});
            var x4 = Vector.Build.Dense(new[] {2.702, 5.146, 0.045});
            var x5 = Vector.Build.Dense(new[] {2.704, 0, 2.148});
            var x6 = Vector.Build.Dense(new[] {2.704, 0, 0.04});
            var x7 = Vector.Build.Dense(new[] {0, 0, 2.134});
            var x8 = Vector.Build.Dense(new[] {0, 0, 0.02});
            var SatPos = Matrix<double>.Build.DenseOfColumnVectors(x1, x2, x3, x4, x5, x6, x7, x8);
            // var arrC = VB.Dense(3, 0.0);
            // var xEst = 
            //     VB.Dense(new []{1, 1, 1, 0, 0, 0, 0, 0, -9.82, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0})
            //         .ToColumnMatrix();
            // var xEst1 = 
            //     VB.Dense(new[] {1/Math.Sqrt(2), 0, 1/Math.Sqrt(2), 0, 0, 0, 0, 0, 0, 0}).ToColumnMatrix();
            // var M1 = MB.Dense(10, 10, 1e-3);
            // var Q1 = (VB.Dense(new[] {1e-3, 1e-3, 1e-3, 1e-11, 1e-11, 1e-11}) * 1e2).ToRowMatrix();
            // var M = MB.Dense(22, 22, 1e-2);
            // var Dra = VB.Dense(new[] {0.1037, 0.0406, 0.0179});
            // var Rn = VB.Dense(new[]
            // {
            //     0.0883 / 1e3, 0.0515 / 1e3, 0.0847 / 1e3, 0.1672 / 1e3, 0.1348 / 1e3, 0.5278 / 1e3, 0.0995 / 1e3,
            //     0.1037, 0.0406, 0.0179, 1e-6, 1e-6, 1e-6, 1e-6
            // }).ToColumnMatrix();
            // var Q = VB.Dense(12, 1e-8).ToColumnMatrix();
            // Q[3, 0] *= 1e3; Q[4, 0] *= 1e3; Q[5, 0] *= 1e3;
            // var xEst2 = VB.Dense(new[] {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -9.8})
                // .ToColumnMatrix();
            // var M2 = MB.Dense(13, 13, 1e-3);
            // var Rn2 = 
            //     VB.Dense(new[] {0.0033, 0.0027, 0.0045, 1e-6, 0.1037, 0.0406, 0.0179, 1e-6, 1e-6, 1e-6})
            //     .ToColumnMatrix();
            // var Q2 = (VB.Dense(new[] {1e-3, 1e-3, 1e-3, 1e-6, 1e-6, 1e-6}) * 1e2).ToRowMatrix();
            // var xEst3 =
            //     VB.Dense(new[] {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -9.8, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0})
            //         .ToColumnMatrix();
            // var M3 = MB.Dense(25, 25, 1e-3);
            // var DRw = VB.Dense(new[] {0.0033, 0.0027, 0.0045});
            // var Rn3 = VB.Dense(new[]
            // {
            //     0.0033, 0.0027, 0.0045, 1e-6, 0.1037 * 1e1, 0.0406 * 1e1, 0.0179 * 1e1, 1e-3, 1e-3, 1e-3, 0.0883 / 5,
            //     0.0515 / 5, 0.0847 / 5, 0.1672 / 5, 0.1348 / 5, 0.5278 / 5, 0.0995 / 5
            // }).ToColumnMatrix();
            // var Q3 =
            //     (VB.Dense(new[] {1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-8, 1e-8, 1e-8}) * 1e2)
            //     .ToColumnMatrix();
            int k = 0;
            // int k_d_p = 0;
            // int k_d_c = 0;

            var x_est5 = MB.DenseOfColumnArrays(new double []{1, 1, 1, 1, 0, 0, 0});
            var M5 = MB.Dense(7, 7, 1.0) / 1e1;
            var Rn5 = MB.Dense(4 + DRn.RowCount, 1);
            for (int i = 0; i < 4; i++)
            {
                Rn5[i, 0] = 1e-6;
            }

            for (int i = 0; i < DRn.RowCount; i++)
            {
                Rn5[i + 4, 0] = DRn[i, 0];
            }

            var Q5 = MB.DenseOfColumnArrays(new double[] {1e-3, 1e-3, 1e-3, 1e-1, 1e-1, 1e-1, 1e-1});
            var q = VB.Dense(new[] {1 / Math.Sqrt(2), 0, 1 / Math.Sqrt(2), 0}).ToColumnMatrix();
            var D_est = VB.Dense(7, 0.0).ToColumnMatrix();
            
            var d = 0.995;
            var gyr2 = MB.Dense(3, 1, 0.0);
            var ddTs = -1.0;
            var pastD = VB.Dense(7, 0.0).ToColumnMatrix();
            Log pastLog = new Log
            {
                Gyro = new double[] {0, 0, 0},
                Acc = new double[] {0, 0, 0}
            };
            using (var sw = new StreamWriter("x_est3_new.csv"))
            {
                using (var sr = new StreamReader("LOG.txt"))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var log = Parser.ParseLogString(line);
                        if (!log.Correct || log.Ts == 0) continue;
                        if (log.Id == "0x4aba")
                        {
                            Matrix<double> D;
                            if (log.DdInclude)
                            {
                                D = MB.DenseOfColumnArrays(log.Dd.SubArray(1, 7));
                                for (int i = 0; i < D.RowCount; ++i)
                                {
                                    if (Math.Abs(D[i, 0]) < TOLERANCE)
                                    {
                                        D[i, 0] = pastD[i, 0];
                                    }
                                }

                                if (k > 100)
                                    raimFlag = true;
                                pastD = D;
                                var dt = ddTs < 0 ? 0.0142 : log.Ts - ddTs;
                                (D_est, DM) = Filters.kalman_dr(D, D_est, DRn, DQ, DM, dt, raimFlag);
                                ddTs = log.Ts;
                                // PrintX(D_est, sw);
                                // Console.WriteLine(D_est);
                                // Console.WriteLine(DM);
                            }
                            else
                            {
                                // PrintX(MB.Dense(D_est.RowCount, D_est.ColumnCount, 0.0), sw);
                            }

                            if (!log.AccInclude)
                                log.Acc = pastLog.Acc;
                            if (!log.GyroInclude)
                                log.Gyro = pastLog.Gyro;
                            if (k > 0)
                            {
                                // var x_est3_temp = MB.Dense(4, 1, 0.0);
                                // for (int i = 0; i < 4; ++i)
                                // {
                                //     x_est3_temp[i, 0] = xEst3[i, 0];
                                // }
                                //
                                // var tempVal = (Quaternion.ToRotationMatrix(xEst3) * MB.DenseOfColumnArrays(log.Acc))
                                //     .PointwiseAbs();
                                // for (int i = 6; i < 9; ++i)
                                // {
                                //     Q3[i, 0] = tempVal[i - 6, 0] / 1e2;
                                // }
                                //
                                // for (int i = 9; i < 12; ++i)
                                // {
                                //     Q3[i, 0] = tempVal[i - 9, 0] / 1e1;
                                // }
                                //
                                // if (Procedures.Norm(log.Gyro) <= 0.3)
                                // {
                                //     Q3[3, 0] = 1;
                                //     Q3[4, 0] = 1;
                                //     Q3[5, 0] = 1;
                                // }
                                // else
                                // {
                                //     Q3[3, 0] = 5e-2;
                                //     Q3[4, 0] = 5e-2;
                                //     Q3[5, 0] = 5e-2;
                                // }

                                d = Procedures.Norm(log.Gyro) <= 0.1 ? 0.0001 : 0.9999;
                                gyr2 = d * gyr2 + (1 - d) * MB.DenseOfColumnArrays(log.Gyro);
                                var gyr1 = MB.DenseOfColumnArrays(log.Gyro) - gyr2;
                                q = q + MB.DenseOfRowArrays(
                                    new [] {0, -gyr1[0, 0], -gyr1[1, 0], -gyr1[2, 0]},
                                    new [] {gyr1[0, 0], 0, gyr1[2, 0], -gyr1[1, 0]},
                                    new [] {gyr1[1, 0], -gyr1[2, 0], 0, gyr1[0, 0]},
                                    new [] {gyr1[2, 0], gyr1[1, 0], -gyr1[0, 0], 0}) * q;
                                var q_norm = q.L2Norm();
                                for (int i = 0; i < q.RowCount; i++)
                                {
                                    q[i, 0] = q[i, 0] /= q_norm;
                                }

                                if (k <= 2e2)
                                {
                                    for (int i = 0; i < 3; i++)
                                    {
                                        Q5[i, 0] = 1e1;
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < 3; i++)
                                    {
                                        Q5[i, 0] = Math.Pow(Procedures.Norm(log.Acc) / 9.81, 6) / 2e2;
                                    }
                                }
                                (x_est5, M5) = Filters.kalman_quat5(D_est, x_est5, Rn5, Q5, M5, log.Ts - pastLog.Ts,
                                log.Acc, log.Gyro, new[] {0, 0, -9.80665}, log.DdInclude, SatPos, q);
                                // SKMNamespace.UdpTest.Test();
                                PrintX(x_est5, sw);
                                Console.WriteLine(k);
                            }
                        }

                        pastLog = log;
                        ++k;
                    }
                }
            }
        }

        public static void PrintX(Matrix<double> x_est, StreamWriter sw)
        {
            for (int i = 0; i < x_est.RowCount; ++i)
            {
                sw.Write(x_est[i, 0] + ";");
            }
            sw.WriteLine();
        }
    }
}